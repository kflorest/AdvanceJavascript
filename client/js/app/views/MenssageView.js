class MessageView extends View{
  constructor(view) {
    super(view);
  }
  template(model) {
    return model.text !== '' ? `<p class="alert alert-info">${model.text}</p>` : `<p></p>`;
  }
}