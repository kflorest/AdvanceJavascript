class NegociacaoView extends View{
  constructor(view) {
    super(view);
  }
  template(model) {
    return `<table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th>DATA</th>
                <th>QUANTIDADE</th>
                <th>VALOR</th>
                <th>VOLUME</th>
            </tr>
        </thead>
        
        <tbody>
          ${
            model.negociacaoList.map((item) =>
              `<tr>
                  <td>${DateHelper.convertToString(item.data)}</td>
                  <td>${item.quantidade}</td>
                  <td>${item.valor}</td>
                  <td>${item.obterVolumen()}</td>
                </tr>`
            ).join('')
          }
        </tbody>
        
        <tfoot>
          <td colspan="3"></td>
          <td>
            ${
              model.negociacaoList.reduce((total, item) => total + item.obterVolumen(), 0.0)
            }
          </td>
        </tfoot>
    </table>`
  }
}