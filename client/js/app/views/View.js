class View {
  constructor(view) {
    this._view = view;
  }
  update(model) {
    this._view.innerHTML = this.template(model);
  }
  template() { // Esta funcao tem que se sobreescrever
    throw new Error(`O metodo template tem que ser implementado.`)
  }
}