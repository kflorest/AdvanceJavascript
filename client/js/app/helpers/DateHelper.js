class DateHelper {
  constructor() {
    throw new Error(`DateHelper nao pode ser instanceada`);
  }
  static convertToDate(data) {
    console.log(data);
    if (!/\d{4}-\d{2}-\d{2}/.test(data)) {
      throw new Error(`Deve ter o formato YYYY-MM-DD`);
    }
    return new Date(
      ...data
      .split(`-`)
      .map((item, key) => item - key % 2 )
    );
  }
  static convertToString(data) {
    return `${data.getDate()}/${data.getMonth() + 1}/${data.getFullYear()}`
  }
}