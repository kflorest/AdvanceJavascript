class NegociacaoController {
  constructor() {

    let $ = document.querySelector.bind(document);
    this._inputData       = $(`#data`);
    this._inputQuantidade = $(`#quantidade`);
    this._inputValor      = $(`#valor`);
    this._mensagem = new Message();
    this._list = new NegociacaoList();
    this._view = new NegociacaoView($(`#negociacao-view`));
    this._view.update(this._list);
    this._messageview = new MessageView($(`#message-view`));
    this._messageview.update(this._mensagem);
  }
  adiciona(e) {
    e.preventDefault();
    
    this._list.adiciona(this._createNegociacao());
    this._view.update(this._list);
    this._mensagem.text = `Mensagem Adicionado com sucesso`;
    this._messageview.update(this._mensagem);
    this._cleanForm();
  }
  _createNegociacao() {
    return new Negociacao(
      DateHelper.convertToDate(this._inputData.value),
      this._inputQuantidade.value,
      this._inputValor.value,
    )
  }
  _cleanForm() {
    this._inputData.value = ``;
    this._inputQuantidade.value = 1;
    this._inputValor.value = 0.0;
    this._inputData.focus();
  }
}