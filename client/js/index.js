var campos = [
  document.querySelector(`#data`),
  document.querySelector(`#quantidade`),
  document.querySelector(`#valor`),
];
var tbody = document.querySelector(`table tbody`);

document.querySelector(`.form`).addEventListener(`submit`, function(e) {
  e.preventDefault();
  var tr = document.createElement(`tr`);
  campos.forEach((campo) => {
    var td = document.createElement(`td`);
    td.textContent = campo.value;
    tr.appendChild(td);
  });

  var tdVolumen = document.createElement(`td`);
  tdVolumen.textContent = campos[1].value * campos[2].value;

  tr.appendChild(tdVolumen);

  tbody.appendChild(tr);

  campos[0].value = ``;
  campos[1].value = 1;
  campos[2].value = 0;
  
  campos[0].focus();

})